/// NOTES:
/// * Change deep sleep time
/// * Test with sensors connected


// Put your libraries here (#include ...)
#include <WaspWIFI_PRO.h>
#include <WaspSensorGas_Pro.h>
#include <WaspFrame.h>


// SERVER settings
///////////////////////////////////////
// WiFi AP settings (CHANGE TO USER'S AP)
///////////////////////////////////////
char ESSID[] = "WiFi_AP_NAME";
char PASSW[] = "WiFi_password";
char slat[] = "";
char slon[] = "";
///////////////////////////////////////
char token[] = "";
char city[] = "";
char type[] = "http";
char host[] = "";
char port[] = "";
char url[]  = "";
uint8_t socket = SOCKET0;
///////////////////////////////////////


// Sensors sockets
///////////////////////////////////////
Gas NO(SOCKET_A);
Gas CO(SOCKET_B);
Gas NO2(SOCKET_C);
Gas SO2(SOCKET_F);
Gas TempHP(SOCKET_E);
///////////////////////////////////////

// Sensor variables
///////////////////////////////////////
float temperature;
float humidity;
float pressure;

float NOconc;
float NOhumid;
float NOtemp;

float NO2conc;
float NO2humid;
float NO2temp;

float COconc;
float COhumid;
float COtemp;

float SO2conc;
float SO2humid;
float SO2temp;
///////////////////////////////////////


// example of body for POST request

uint8_t error;
uint8_t status;
unsigned long previous;

void send_data(
  float temperature,
  float humidity,
  float pressure,
  float NOconc,
  float NOhumid,
  float NOtemp,
  float NO2conc,
  float NO2humid,
  float NO2temp,
  float COconc,
  float COhumid,
  float COtemp,
  float SO2conc,
  float SO2humid,
  float SO2temp
);


void setup()
{
  // 1. Switch ON the WiFi module
  error = WIFI_PRO.ON(socket);

  if (error == 0){
  USB.println(F("1. WiFi switched ON"));
  }
  else{
    USB.println(F("1. WiFi did not initialize correctly"));
  }

  // 2. Reset to default values
  error = WIFI_PRO.resetValues();

  if (error == 0){
  USB.println(F("2. WiFi reset to default"));
  }
  else{
  USB.println(F("2. WiFi reset to default ERROR"));
  }

  // 3. Set ESSID
  error = WIFI_PRO.setESSID(ESSID);

  if (error == 0){
  USB.println(F("3. WiFi set ESSID OK"));
  }
  else{
  USB.println(F("3. WiFi set ESSID ERROR"));
  }

  error = WIFI_PRO.setPassword(WPA2, PASSW);
  if (error == 0){
  USB.println(F("4. WiFi set AUTHKEY OK"));
  }
  else{
  USB.println(F("4. WiFi set AUTHKEY ERROR"));
  }

  error = WIFI_PRO.softReset();

  if (error == 0){
  USB.println(F("5. WiFi softReset OK"));
  }
  else{
  USB.println(F("5. WiFi softReset ERROR"));
  }

  NO.ON();
  NO2.ON();
  CO.ON();
  SO2.ON();
  TempHP.ON();

  // warming up sensors for 10 minutes...
  PWR.deepSleep("00:00:10:00", RTC_OFFSET, RTC_ALM1_MODE1, ALL_ON);


}



void loop()
{
  error = WIFI_PRO.ON(socket);

  if (error == 0){
    USB.println(F("1. WiFi switched ON"));
  }
  else{
    USB.println(F("1. WiFi did not initialize correctly"));
  }

  // Read the sensors and compensate with the temperature internally
  NOconc = NO.getConc();
  NOhumid = NO.getHumidity();
  NOtemp = NO.getTemp();

  NO2conc = NO2.getConc();
  NO2humid = NO2.getHumidity();
  NO2temp = NO2.getTemp();

  COconc = CO.getConc();
  COhumid = CO.getHumidity();
  COtemp = CO.getTemp();

  SO2conc = SO2.getConc();
  SO2humid = SO2.getHumidity();
  SO2temp = SO2.getTemp();
  // Read enviromental variables
  temperature = TempHP.getTemp();
  humidity = TempHP.getHumidity();
  pressure = TempHP.getPressure();

  ///////////////////////////////////////////
  // 4. Sleep
  ///////////////////////////////////////////
  float gas_values[] = {temperature, humidity, pressure,
    COconc,
    COhumid,
    COtemp,
    NOconc,
    NOhumid,
    NOtemp,
    NO2conc,
    NO2humid,
    NO2temp,
    SO2conc,
    SO2humid,
    SO2temp,
  };

  char* gas_names[] = {"temp", "hum", "pres", "CO", "h", "t", "NO", "h", "t", "NO2", "h", "t", "SO2", "h", "t"};

  for (int i=0; i < 15; i++){
      USB.print(gas_names[i]);
      USB.print(": ");
      USB.println(gas_values[i]);
  }

  send_data(gas_values); // sending data to server

  // Go to deepsleep
  // After 30 seconds, Waspmote wakes up thanks to the RTC Alarm
  WIFI_PRO.OFF(socket);
  USB.println(F("4. WiFi switched OFF\n\n"));

  // looping every 2 minutes
  PWR.deepSleep("00:00:02:00", RTC_OFFSET, RTC_ALM1_MODE1, ALL_OFF);
}


void send_data(float gas_values[]){

  // sends data to server
  ///////////////////////////////
  char float_tmp[10];
  char float_hum[10];
  char float_prs[10];

  char float_CO[20];
  char float_NO[20];
  char float_NO2[20];
  char float_SO2[20];

  char float_CO_hum[10];
  char float_NO_hum[10];
  char float_NO2_hum[10];
  char float_SO2_hum[10];

  char float_CO_temp[10];
  char float_NO_temp[10];
  char float_NO2_temp[10];
  char float_SO2_temp[10];

  char message[240];

  error = WIFI_PRO.setURL( type, host, port, url );

  if (error == 0){
    USB.println(F("2. setURL OK"));
  }
  else{
    USB.println(F("2. Error calling 'setURL' function"));
    WIFI_PRO.printErrorCode();
  }

  status =  WIFI_PRO.isConnected();

  if (status == true){
    USB.print(F("3. WiFi is connected OK"));
    dtostrf(gas_values[0], 1, 2, float_tmp);
    dtostrf(gas_values[1], 1, 2, float_hum);
    dtostrf(gas_values[2], 1, 0, float_prs);
    dtostrf(gas_values[3], 1, 4, float_CO);
    dtostrf(gas_values[4], 1, 0, float_CO_hum);
    dtostrf(gas_values[5], 1, 0, float_CO_temp);

    dtostrf(gas_values[6], 1, 4, float_NO);
    dtostrf(gas_values[7], 1, 0, float_NO_hum);
    dtostrf(gas_values[8], 1, 0, float_NO_temp);

    dtostrf(gas_values[9], 1, 4, float_NO2);
    dtostrf(gas_values[10], 1, 0, float_NO2_hum);
    dtostrf(gas_values[11], 1, 0, float_NO2_temp);

    dtostrf(gas_values[12], 1, 4, float_SO2);
    dtostrf(gas_values[13], 1, 0, float_SO2_hum);
    dtostrf(gas_values[14], 1, 0, float_SO2_temp);

    USB.println(F("\n----------------------------------------"));
    USB.println(F("Request body: "));
    USB.println(F("----------------------------------------"));
    snprintf( message, sizeof(message),
    "token=%s&city=%s&lat=%s&lon=%s&temperature=%s&humidity=%s&pressure=%s&CO=%s&CO_h=%s&CO_t=%s&NO=%s&NO_h=%s&NO_t=%s&NO2=%s&NO2_h=%s&NO2_t=%s&SO2=%s&SO2_h=%s&SO2_t=%s",
    token, city, slat, slon,
    float_tmp, float_hum, float_prs,
    float_CO, float_CO_hum, float_CO_temp,
    float_NO, float_NO_hum, float_NO_temp,
    float_NO2, float_NO2_hum, float_NO2_temp,
    float_SO2, float_SO2_hum, float_SO2_temp
    );
    USB.println(message);
    error = WIFI_PRO.post(message);
    // check response
    if (error == 0){
      USB.print(F("3.1. HTTP POST OK. "));
      USB.print(F("\nServer answer:"));
      USB.println(WIFI_PRO._buffer, WIFI_PRO._length);
    }
    else{
      USB.println(F("3.1. Error calling 'post' function"));
      WIFI_PRO.printErrorCode();
    }
  }
  else{
    USB.print(F("3. WiFi is connected ERROR"));
    }
}
